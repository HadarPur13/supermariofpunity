﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HoleScript : MonoBehaviour
{
    public AudioSource DeathSound;
    public GameObject LevelMusic;
    public bool isEnter = false;

    private IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && !isEnter)
        {
            isEnter = true;
            PlayerScript.stillAlive = false;
            GlobalLives.livesAmount -= 1;
            LevelMusic.SetActive(false);
            DeathSound.Play();
            yield return new WaitForSeconds(3);
            GlobalScore.currenScore = 0;
            GlobalCoins.coinsCount = 0;
            SceneManager.LoadScene(LevelCounted.scenePreloadCount);
        }

        else if (other.gameObject.tag == "Enemy")
        {
            other.GetComponent<EnemyScript>().Dead();
        }
    }
}
