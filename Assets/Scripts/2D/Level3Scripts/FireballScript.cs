﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireballScript : MonoBehaviour
{
    public Transform pivot;
    public float rotateSpeed = 75;
    private bool canMove = false;
    private bool hit = false;

    public GameObject player;
    public AudioSource DeathSound;
    public GameObject LevelMusic;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        if (canMove && !hit)
        {
            transform.RotateAround(pivot.position, Vector3.forward, rotateSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            hit = true;
            StartCoroutine(Death());
        }
    }

    private void OnBecameVisible()
    {
        canMove = true;
    }

    private IEnumerator Death()
    {
        PlayerScript.stillAlive = false;

        player.GetComponent<Animator>().SetBool("isJumping", false);
        player.GetComponent<Animator>().SetBool("isRunning", false);

        player.GetComponent<Collider2D>().enabled = false;

        GlobalLives.livesAmount -= 1;
        LevelMusic.SetActive(false);
        DeathSound.Play();
        yield return new WaitForSeconds(3);
        GlobalScore.currenScore = 0;
        GlobalCoins.coinsCount = 0;
        SceneManager.LoadScene(LevelCounted.scenePreloadCount);

    }
}
