﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe002Entry : MonoBehaviour
{
    public int StoodOn;

    public GameObject mainPlayer;
    public GameObject mainCam;
    public GameObject secCam;
    public GameObject topColl;

    public GameObject fadeScreen;
    public AudioSource pipeEffect;
    public AudioSource pipeEffectOut;

    public float downVelocity;
    public float gravity;
    public bool isDown = false;

    private void OnTriggerStay2D(Collider2D collision)
    {
        StoodOn = 1;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        StoodOn = 1;
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        StoodOn = 0;
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (StoodOn == 1 && !isDown)
            {
                StartCoroutine(PlayAnim());
            }
        }
    }

    IEnumerator PlayAnim()
    {
        mainCam.GetComponent<CameraFollow>().SetKeepFollow(false);

        isDown = true;
        pipeEffect.Play();
        topColl.GetComponent<Collider2D>().enabled = false;
        fadeScreen.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        mainPlayer.SetActive(false);
        yield return new WaitForSeconds(1f);
        fadeScreen.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(0.495f);
        fadeScreen.GetComponent<Animator>().enabled = false;
        secCam.SetActive(true);
        mainCam.transform.position = new Vector3(78, 7.5f, -10);
        mainCam.SetActive(false);
        yield return new WaitForSeconds(0.495f);
        fadeScreen.GetComponent<Animator>().enabled = false;
        fadeScreen.SetActive(false);
        mainPlayer.SetActive(true);
        mainPlayer.transform.position = new Vector3(111, 75, 0);
        topColl.GetComponent<Collider2D>().enabled = true;
        pipeEffectOut.Play();
    }
}
