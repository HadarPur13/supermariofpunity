﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Vector2 velocity;

    public LayerMask wallMask;
    public LayerMask floorMask;
    public float bounceVelocity;
    public float jumpVelocity;
    public float gravity;
    public AudioSource jumpEffect;

    private float deadVelocity = 30;
    private bool walk, walkLeft, walkRight, jump;
    private bool bounce = false; 
    public static bool stillAlive = true;

    public enum PlayerState
    {
        Jumping, 
        Idle,
        Walking,
        Bouncing, 
        Die
    }

    private PlayerState playerState = PlayerState.Idle;
    private bool grounded = false;

    // Update is called once per frame
    void Update()
    {
        if (stillAlive)
        {
            CheckPlayerInput();
            UpdatePlayerPos();
            UpdateAnimationState();
        }
        else
        {
            GetComponent<Animator>().SetBool("isJumping", false);
            GetComponent<Animator>().SetBool("isRunning", false);
            GetComponent<Animator>().SetBool("isDead", true);

            Vector3 pos = transform.localPosition;

            pos.y += deadVelocity * Time.deltaTime;
            deadVelocity -= gravity * Time.deltaTime;

            transform.localPosition = new Vector3(pos.x, pos.y, 2);
        }
    }

    void UpdatePlayerPos()
    {
        Vector3 pos = transform.localPosition;
        Vector3 scale = transform.localScale;

        if (walk)
        {
            if (walkLeft)
            {
                pos.x -= velocity.x * Time.deltaTime;
                scale.x = -1;
            }

            if (walkRight)
            {
                pos.x += velocity.x * Time.deltaTime;
                scale.x = 1;
            }

            pos = CheckWallRays(pos, scale.x);
        }

        if (jump && playerState != PlayerState.Jumping)
        {
            jumpEffect.Play();
            playerState = PlayerState.Jumping;
            velocity = new Vector2(velocity.x, jumpVelocity);
        }

        if (playerState == PlayerState.Jumping)
        {
            pos.y += velocity.y * Time.deltaTime;
            velocity.y -= gravity * Time.deltaTime;
        }

        if (bounce && playerState != PlayerState.Bouncing)
        {
            playerState = PlayerState.Bouncing;
            velocity = new Vector2(velocity.x, bounceVelocity);
        }

        if (playerState == PlayerState.Bouncing)
        {
            pos.y += velocity.y * Time.deltaTime;
            velocity.y -= gravity * Time.deltaTime;
        }

        if (velocity.y <= 0) pos = CheckFloorRays(pos);
        if (velocity.y >= 0) pos = CheckCeilingRays(pos);

        transform.localPosition = pos;
        transform.localScale = scale;
    }

    void UpdateAnimationState()
    {
        if (grounded && !walk && !bounce)
        {
            GetComponent<Animator>().SetBool("isJumping", false);
            GetComponent<Animator>().SetBool("isRunning", false);
        }

        if (grounded && walk)
        {
            GetComponent<Animator>().SetBool("isJumping", false);
            GetComponent<Animator>().SetBool("isRunning", true);
        }

        if (playerState == PlayerState.Jumping)
        {
            GetComponent<Animator>().SetBool("isJumping", true);
            GetComponent<Animator>().SetBool("isRunning", false);
        }
    }

    void CheckPlayerInput()
    {
        bool inputLeft = Input.GetKey(KeyCode.LeftArrow);
        bool inputRight = Input.GetKey(KeyCode.RightArrow);
        bool inputJump = Input.GetKeyDown(KeyCode.Space);

        walk = inputLeft || inputRight;
        walkLeft = inputLeft && !inputRight;
        walkRight = inputRight && !inputLeft;
        jump = inputJump;
    }

    Vector3 CheckWallRays(Vector3 pos, float dir)
    {
        Vector2 originTop = new Vector2(pos.x + dir * .4f, pos.y + 1f - 0.2f);
        //pos.x is the center of the player, player is 1 unit width
        //so pos.x + dir * .4f we get .1f inside of the player

        //pos.y is the center of the player, player is 2 unit height
        //so pos.y + 1f - 0.2f we get .2f inside of the player from his head

        Vector2 originMiddle = new Vector2(pos.x + dir * .4f, pos.y);
        //pos.x is the center of the player, player is 1 unit width
        //so pos.x + dir * .4f we get .1f inside of the player

        Vector2 originBottom = new Vector2(pos.x + dir * .4f, pos.y - 1f + 0.2f);
        //pos.x is the center of the player, player is 1 unit width
        //so pos.x + dir * .4f we get .1f inside of the player

        //pos.y is the center of the player, player is 2 unit height
        //so pos.y - 1f + 0.2f we get .2f inside of the player from his legs


        RaycastHit2D wallTop = Physics2D.Raycast(originTop, new Vector2(dir, 0), velocity.x * Time.deltaTime, wallMask);
        RaycastHit2D wallMiddle = Physics2D.Raycast(originMiddle, new Vector2(dir, 0), velocity.x * Time.deltaTime, wallMask);
        RaycastHit2D wallBottom = Physics2D.Raycast(originBottom, new Vector2(dir, 0), velocity.x * Time.deltaTime, wallMask);

        if (wallTop.collider != null || wallMiddle.collider != null || wallBottom.collider != null)
        {
            pos.x -= velocity.x * Time.deltaTime * dir;
        }

        return pos;
    }

    Vector3 CheckFloorRays(Vector3 pos)
    {
        Vector2 originLeft = new Vector2(pos.x - 0.5f + 0.2f, pos.y - 1f);
        Vector2 originMiddle = new Vector2(pos.x , pos.y - 1f);
        Vector2 originRight = new Vector2(pos.x + 0.5f - 0.2f, pos.y - 1f);

        RaycastHit2D floorLeft = Physics2D.Raycast(originLeft, Vector2.down, velocity.y * Time.deltaTime, floorMask);
        RaycastHit2D floorMiddle = Physics2D.Raycast(originMiddle, Vector2.down, velocity.y * Time.deltaTime, floorMask);
        RaycastHit2D floorRight = Physics2D.Raycast(originRight, Vector2.down, velocity.y * Time.deltaTime, floorMask);

        if (floorLeft.collider != null || floorMiddle.collider != null || floorRight.collider != null)
        {
            RaycastHit2D hitRay = floorRight;

            if (floorLeft) hitRay = floorLeft;
            else if (floorMiddle) hitRay = floorMiddle;
            else if (floorRight) hitRay = floorRight;

            if (hitRay.collider.tag == "Enemy")
            {
                bounce = true;
                hitRay.collider.GetComponent<EnemyScript>().Crash();
            }

            playerState = PlayerState.Idle;
            grounded = true;
            velocity.y = 0;

            pos.y = hitRay.collider.bounds.center.y + hitRay.collider.bounds.size.y / 2 + 1;
        } 
        else
        {
            if (playerState != PlayerState.Jumping)
            {
                Fall();
            }
        }

        return pos;
    }

    Vector3 CheckCeilingRays(Vector3 pos)
    {
        Vector2 originLeft = new Vector2(pos.x - 0.5f + 0.2f, pos.y + 1f);
        Vector2 originMiddle = new Vector2(pos.x, pos.y + 1f);
        Vector2 originRight = new Vector2(pos.x + 0.5f - 0.2f, pos.y + 1f);

        RaycastHit2D cielLeft = Physics2D.Raycast(originLeft, Vector2.up, velocity.y * Time.deltaTime, floorMask);
        RaycastHit2D cielMiddle = Physics2D.Raycast(originMiddle, Vector2.up, velocity.y * Time.deltaTime, floorMask);
        RaycastHit2D cielRight = Physics2D.Raycast(originRight, Vector2.up, velocity.y * Time.deltaTime, floorMask);

        if (cielLeft.collider != null || cielMiddle.collider != null || cielRight.collider != null)
        {
            RaycastHit2D hitRay = cielLeft;

            if (cielLeft) hitRay = cielLeft;
            else if (cielMiddle) hitRay = cielMiddle;
            else if (cielRight) hitRay = cielRight;

            if (hitRay.collider.tag == "QuestionBlock")
            {
                hitRay.collider.GetComponent<QBlockScript>().QBlockBounced();
            }

            else if (hitRay.collider.tag == "Block")
            {
                hitRay.collider.GetComponent<BlockScript>().QBlockBounced();
            }

            pos.y = hitRay.collider.bounds.center.y - hitRay.collider.bounds.size.y / 2 - 1;
            Fall();
        }

        return pos;
    }

    public void Fall()
    {
        velocity.y = 0;
        playerState = PlayerState.Jumping;
        bounce = false;
        grounded = false;
    }
}
