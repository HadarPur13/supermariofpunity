﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QBlock002 : MonoBehaviour
{
    public GameObject deadBlock;
    public GameObject qBlock;
    public GameObject coin;
    public bool coinOut = false;
    public AudioSource coinsEffect;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (!coinOut)
        {
            coinOut = true;
            qBlock.SetActive(false);
            deadBlock.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            coin.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            Destroy(coin);
            coinsEffect.Play();
            GlobalCoins.coinsCount += 1;
            GlobalScore.currenScore += 10;
        }
    }
}
