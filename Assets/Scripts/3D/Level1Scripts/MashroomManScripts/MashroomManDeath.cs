﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MashroomManDeath : MonoBehaviour
{
    public GameObject mashroomMan;
    public GameObject mashroomManLeftColl;
    public GameObject mashroomManRightColl;
    public GameObject mashroomManTopColl;
    public GameObject mashroomManBottomColl;

    public static bool isCrashed = false;
    public AudioSource DeathSound;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isCrashed = true;
            DeathSound.Play();
            GetComponent<BoxCollider>().enabled = false;

            mashroomManLeftColl.GetComponent<Collider>().enabled = false;
            mashroomManRightColl.GetComponent<Collider>().enabled = false;
            mashroomManTopColl.GetComponent<Collider>().enabled = false;
            mashroomManBottomColl.GetComponent<Collider>().enabled = false;

            mashroomMan.GetComponent<MashroomManMovement>().enabled = false;
            mashroomMan.transform.localScale -= new Vector3(0, 0, 0.4f);
            mashroomMan.transform.localPosition -= new Vector3(0, 0, 0.2f);
            GlobalScore.currenScore += 100;
            yield return new WaitForSeconds(1);
            Destroy(mashroomMan);
        }
    }
}
