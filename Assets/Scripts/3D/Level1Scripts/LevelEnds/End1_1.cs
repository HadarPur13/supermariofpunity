﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class End1_1 : MonoBehaviour
{
    public GameObject fadeScreen;
    public GameObject player;

    public AudioSource levelCompleteEffect;
    public AudioSource levelEffect;

    private int timeScore;
    private int timeLeft;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            levelEffect.Stop();
            levelCompleteEffect.Play();
            fadeScreen.SetActive(true);
            player.SetActive(false);
            yield return new WaitForSeconds(8f);
            fadeScreen.GetComponent<Animator>().enabled = true;
            timeLeft = CountingDown.TimeLeft;
            timeScore = timeLeft * 10;
            GlobalScore.currenScore += timeScore;
            yield return new WaitForSeconds(0.495f);
            fadeScreen.GetComponent<Animator>().enabled = false;
            
            SceneManager.LoadScene(LevelCounted.scenePreloadCount+2);
        }
    }
}
