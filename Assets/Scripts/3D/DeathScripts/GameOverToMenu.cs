﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverToMenu : MonoBehaviour
{
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(5.8f);
        SceneManager.LoadScene(0);
    }
}
