﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneAI : MonoBehaviour
{

    public Transform Player;
    int MoveSpeed = 4;
    int MinDist = 0;

    void Update()
    {
        if (StoneTrigger.isStoneTriggered && !StoneStopTrigger.isStoneStopTriggered)
        {
            transform.LookAt(Player);

            GetComponent<Rigidbody>().useGravity = true;
            var dist = Vector3.Distance(transform.position, Player.position);
            if (dist >= MinDist)
            {
                transform.position += transform.forward * MoveSpeed * Time.deltaTime;
            }
        }
    }
}
