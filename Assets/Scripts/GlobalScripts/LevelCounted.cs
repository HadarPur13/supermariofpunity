﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCounted : MonoBehaviour
{
    public GameObject levelDisplay;
    public static int levelCount;
    public static int scenePreloadCount;

    public int internalLevelCount;
    public int internalSceneCount;

    private void Update()
    {
        internalLevelCount = levelCount;
        internalSceneCount = scenePreloadCount;
        levelDisplay.GetComponent<Text>().text = "World 1-" + levelCount;
    }
}
