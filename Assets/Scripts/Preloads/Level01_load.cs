﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Level01_load : MonoBehaviour
{
    IEnumerator Start()
    {
        LevelCounted.levelCount = 1;
        LevelCounted.scenePreloadCount = 1;
        yield return new WaitForSeconds(5);
        PlayerScript.stillAlive = true;
        SceneManager.LoadScene(2);
    }
}
