# Super Mario Final Project - Unity

Super Mario is a 2D and 3D game that was developed as part of the Unity course's final project. 

Super Mario is a series of platform games created by Nintendo, and featuring their mascot, Mario.
Trough the game, Mario needs to collect coins, break blocks and to avoid the obstacles all over the game to finish and win.
The game contains 2 2D levels and 2 3D levels, the player get 3 lives in the beginning and there are red mushrooms that can guarantee lives and green mushrooms that increase his size so he could break the blocks.
Also, there are Goombas, Fireballs and obstacles that can kill our lovely Mario.  

## Game video:

[![Video](mario.png)](https://drive.google.com/open?id=1VMkeBol8rQzpg0TTMFL6UiDw4lqxQT2z "MarioUnityGame")

